# Канал уведомлений в Telegram для Laravel

[![pipeline status](https://gitlab.com/mihakot/notification-telegram/badges/master/pipeline.svg)](https://gitlab.com/mihakot/notification-telegram/-/commits/master)
[![Latest Version on Packagist](https://img.shields.io/packagist/v/mihakot/notification-telegram.svg?style=flat-square)](https://packagist.org/packages/mihakot/notification-telegram)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Total Downloads](https://img.shields.io/packagist/dt/mihakot/notification-telegram.svg?style=flat-square)](https://packagist.org/packages/mihakot/notification-telegram)

[TOC]

## Настройка

### Установка пакета

Установка пакета через composer:

```bash
composer require mihakot/notification-telegram
```

При необходимости можно опубликовать файл конфигурации

```bash
@php artisan vendor:publish --vendor NotificationChannels\Telegram\TelegramServiceProvider
```

### Настройка вашего Telegram Bot

Зарегистрировать бота через [@BotFather](https://core.telegram.org/bots#6-botfather) и получить Bot API Token.

Затем сконфигурировать его в .env вашего приложения

```dotenv
TELEGRAM_API_TOKEN={YOUR_API_TOKEN}
```

Данная переменная используется в файле конфигурации, но вы можете использовать другие значения в момент вызова метода.

```php
\NotificationChannels\Telegram\Telegram::message()
->setToken('ВАШ_ТОКЕН')
```

### Получение Chat ID

Для отправки уведомлений требуется знать Chat ID.

Получить его можно через webhook или через [`Telegram::updates()`](#получение-последних-данных)



## Использование

Теперь можете использовать канал `telegram` в методе `via()` внутри класса `Notification`.

```php
# пример использования

use Illuminate\Notifications\Notification;

class NotifyUser extends Notification
{
    public function via($notifiable)
    {
        return ["telegram"];
    }

    public function toTelegram($notifiable)
    {
        return \NotificationChannels\Telegram\Telegram::message()
            // Получатель
            ->to($notifiable->telegram_chat_id)
            ->content("Hello there!")
            ->line("Your are welcome!")
            ->line("Thank you!")
    }
}
```

**Общедоступные методы**

```php 
// установка токена
->setToken() 

// установка адреса api
->setApiBaseUri() 
 
// получатель события
->to()

// добавление кнопки с действием к сообщению
->button()

// добавление дополнительных опций
->options() 
```  

### Получение последних данных

```php
\NotificationChannels\Telegram\Telegram::updates()
->get() 
```

**Доступные методы**

[Telegram API getUpdates](https://core.telegram.org/bots/api#getupdates)

```php
# количество получаемых событий 
->limit()

# получаемые события, номер последнего получаемого события (последнее полученное + 1)  
->latest()

# получить события
->get()
````  

### Отправка сообщения

```php
\NotificationChannels\Telegram\Telegram::message()
->to('Chat ID')
->content('YOUR_MESSAGE')
->send() 
```

**Доступные методы**

```php
# текст уведомления
->content()

# добавление строки к уведомлению 
->line()

# добавление экранированной строки к уведомлению
->escapedLine()

# добавление blade шаблона в качестве уведомления
->view()

# отправка сообщения
->send()
```` 

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email mihakot@gmail.com instead of using the issue tracker.

## Credits

- [Konstantin "MihaKot" Aksarin](https://gitlab.com/mihakot)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
