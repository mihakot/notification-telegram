<?php
return [
    'api' => [
        'token'    => env('TELEGRAM_API_TOKEN', ''),
        'base_uri' => env('TELEGRAM_API_HOST', 'https://api.telegram.org'),
    ]
];