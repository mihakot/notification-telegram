# Changelog

All notable changes to `notification-telegram` will be documented in this file

## 0.0.1 - 07/10/2023

- initial release

## 0.2.1 - 13/10/2023

- get updates
- send message

## 0.2.2 - 14/10/2023

- some text in README

## 0.2.4 - 23/10/2023
- add docs generated by phpDocumentor
- update README
- 
## 0.2.6 - 03/03/2025
- update supported versions 
