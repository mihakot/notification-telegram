<?php

namespace NotificationChannels\Telegram\Objects;

use Illuminate\Support\Facades\View;
use NotificationChannels\Telegram\Contracts\TelegramSenderContract;
use NotificationChannels\Telegram\Telegram;
use function mb_str_split;
use function mb_strwidth;

/**
 * Class Telegram\Objects\Message
 *
 * @package NotificationChannels\Telegram
 * @author  Konstantin #MihaKot# Aksarin  <mihakot@gmail.com>
 */
class Message extends Telegram implements TelegramSenderContract
{
    /** @var int Message Chunk Size */
    public int $chunkSize = 0;

    public function __construct(string $content = '')
    {
        parent::__construct();
        $this->content($content);
        $this->payload['parse_mode'] = 'Markdown';
    }

    /**
     * Notification message (Supports Markdown)
     *
     * @param  string    $content
     * @param  int|null  $limit
     *
     * @return $this
     */
    public function content(string $content, int $limit = null): self
    {
        $this->payload['text'] = $content;

        if ($limit) {
            $this->chunkSize = $limit;
        }

        return $this;
    }

    /**
     * Addon line to text
     *
     * @param  string  $content
     *
     * @return $this
     */
    public function line(string $content): self
    {
        $this->payload['text'] .= $content."\n";

        return $this;
    }

    /**
     * Addon escaped line to text
     *
     * @param  string  $content
     *
     * @return $this
     */
    public function escapedLine(string $content): self
    {
        // code taken from public gist https://gist.github.com/vijinho/3d66fab3270fc377b8485387ce7e7455
        $content = str_replace([
            '\\',
            '-',
            '#',
            '*',
            '+',
            '`',
            '.',
            '[',
            ']',
            '(',
            ')',
            '!',
            '&',
            '<',
            '>',
            '_',
            '{',
            '}',
        ], [
            '\\\\',
            '\-',
            '\#',
            '\*',
            '\+',
            '\`',
            '\.',
            '\[',
            '\]',
            '\(',
            '\)',
            '\!',
            '\&',
            '\<',
            '\>',
            '\_',
            '\{',
            '\}',
        ], $content);

        return $this->line($content);
    }

    /**
     * Attach a view file as the content for the notification.
     * Supports Laravel blade template.
     *
     * @param  string  $view
     * @param  array   $data
     * @param  array   $mergeData
     *
     * @return $this
     */
    public function view(string $view, array $data = [], array $mergeData = []): self
    {
        return $this->content(View::make($view, $data, $mergeData)->render());
    }

    /**
     * Chunk message to given size.
     *
     * @param  int  $limit
     *
     * @return $this
     */
    public function chunk(int $limit = 4096): self
    {
        $this->chunkSize = $limit;

        return $this;
    }

    /**
     * Should message be chunked
     *
     * @return bool
     */
    public function shouldChunk(): bool
    {
        return $this->chunkSize > 0;
    }

    /**
     * Send text message
     *
     * <code>
     * $params = [
     *   'chat_id'                  => '',
     *   'text'                     => '',
     *   'parse_mode'               => '',
     *   'disable_web_page_preview' => '',
     *   'disable_notification'     => '',
     *   'reply_to_message_id'      => '',
     *   'reply_markup'             => '',
     * ];
     * </code>
     */
    public function send()
    {
        if ($this->shouldChunk()) {
            return $this->sendChunkedMessage($this->payload);
        }

        return $this->sendRequest('sendMessage', $this->payload);
    }


    /**
     * @param  array  $params
     *
     * @return array
     */
    private function sendChunkedMessage(array $params): array
    {
        $replyMarkup = $this->getPayloadValue('reply_markup');

        if ($replyMarkup) {
            unset($params['reply_markup']);
        }

        $messages = $this->chunkStrings($this->getPayloadValue('text'), $this->chunkSize);

        $payloads = collect($messages)
            ->filter()
            ->map(fn($text) => array_merge($params, ['text' => $text]));

        if ($replyMarkup) {
            $lastMessage = $payloads->pop();
            $lastMessage['reply_markup'] = $replyMarkup;
            $payloads->push($lastMessage);
        }

        return $payloads->map(function ($payload) {
            $response = $this->sendRequest('sendMessage', $payload);
            // To avoid rate limit of one message per second.
            sleep(1);

            if ($response) {
                return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
            }

            return $response;
        })->toArray();
    }

    /**
     * Chunk the given string into an array of strings.
     */
    private function chunkStrings(string $value, int $limit = 4096): array
    {
        if (mb_strwidth($value, 'UTF-8') <= $limit) {
            return [$value];
        }

        if ($limit > 4096) {
            $limit = 4096;
        }

        $output = explode('%#TGMSG#%', wordwrap($value, $limit, '%#TGMSG#%'));

        // Fallback for when the string is too long and wordwrap doesn't cut it.
        if (count($output) <= 1) {
            $output = mb_str_split($value, $limit, 'UTF-8');
        }

        return $output;
    }
}
