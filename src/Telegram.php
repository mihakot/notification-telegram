<?php

namespace NotificationChannels\Telegram;

use Exception;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use JsonSerializable;
use NotificationChannels\Telegram\Exceptions\CouldNotSendNotification;
use NotificationChannels\Telegram\Objects\Message;
use NotificationChannels\Telegram\Objects\Updates;
use NotificationChannels\Telegram\Traits\HasSharedLogic;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Telegram
 *
 * @package NotificationChannels\Telegram
 * @author  Konstantin #MihaKot# Aksarin  <mihakot@gmail.com>
 */
class Telegram implements JsonSerializable
{
    use HasSharedLogic;

    /** @var HttpClient GuzzleHttp HTTP Client */
    protected HttpClient $http;

    /** @var null|string Telegram Bot API Token. */
    protected ?string $token;

    /** @var string Telegram Bot API Base URI */
    protected string $apiBaseUri;

    public function __construct()
    {
        $this->setToken(config('telegram.api.token'));
        $this->setApiBaseUri(config('telegram.api.base_uri'));
        $this->setHttpClient($httpClient ?? new HttpClient());
    }

    /**
     * Telegram updates
     *
     * @return Updates
     */
    public static function updates(): Updates
    {
        return new Updates;
    }

    /**
     * Telegram messages
     *
     * @return Message
     */
    public static function message(): Message
    {
        return new Message;
    }

    /**
     * Token getter.
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * Token setter.
     *
     * @return $this
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * API Base URI getter.
     */
    public function getApiBaseUri(): string
    {
        return $this->apiBaseUri;
    }

    /**
     * set API Base URI.
     *
     * @return $this
     */
    public function setApiBaseUri(string $apiBaseUri): self
    {
        $this->apiBaseUri = trim($apiBaseUri, '/');

        return $this;
    }

    /**
     * Set HTTP Client.
     *
     * @return $this
     */
    public function setHttpClient(HttpClient $http): self
    {
        $this->http = $http;

        return $this;
    }

    /**
     * Get HttpClient.
     */
    protected function httpClient(): HttpClient
    {
        return $this->http;
    }


    /**
     * Send an API request and return response.
     *
     * @param string $endpoint
     * @param array  $params
     * @param bool   $multipart
     * @return \Psr\Http\Message\ResponseInterface
     * @throws CouldNotSendNotification
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    protected function sendRequest(string $endpoint, array $params, bool $multipart = false): ResponseInterface
    {
        if(empty($this->getToken())) {
            throw CouldNotSendNotification::telegramBotTokenNotProvided();
        }

        $apiUri = sprintf('%s/bot%s/%s', $this->getApiBaseUri(), $this->getToken(), $endpoint);

        try {
            return $this->httpClient()->post($apiUri, [
                $multipart ? 'multipart' : 'form_params' => $params,
            ]);
        } catch (ClientException $exception) {
            throw CouldNotSendNotification::telegramRespondedWithAnError($exception);
        } catch (Exception $exception) {
            throw CouldNotSendNotification::couldNotCommunicateWithTelegram($exception);
        }
    }
}
