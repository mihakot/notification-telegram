<?php

namespace NotificationChannels\Telegram\Traits;

use Illuminate\Support\Traits\Conditionable;

/**
 * Trait  Telegram\Trait\HasSharedLogic
 *
 * @package NotificationChannels\Telegram
 * @author  Konstantin #MihaKot# Aksarin  <mihakot@gmail.com>
 */
trait HasSharedLogic
{
    use Conditionable;

    /** @var array Params payload */
    protected array $payload = [];

    /** @var array Inline Keyboard Buttons */
    protected array $buttons = [];

    /**
     * Recipient's Chat ID.
     *
     * @param  int|string  $chatId
     *
     * @return static
     */
    public function to($chatId): self
    {
        $this->payload['chat_id'] = $chatId;

        return $this;
    }

    /**
     * Add an inline button
     *
     * @param  string  $text
     * @param  string  $url
     * @param  int     $columns
     *
     * @return static
     * @throws \JsonException
     */
    public function button(string $text, string $url, int $columns = 2): self
    {
        $this->buttons[] = compact('text', 'url');

        $this->payload['reply_markup'] = json_encode([
            'inline_keyboard' => array_chunk($this->buttons, $columns),
        ], JSON_THROW_ON_ERROR);

        return $this;
    }

    /**
     * Add an inline button with callback_data
     *
     * @param  string  $text
     * @param  string  $callback_data
     * @param  int     $columns
     *
     * @return static
     * @throws \JsonException
     */
    public function buttonWithCallback(string $text, string $callback_data, int $columns = 2): self
    {
        $this->buttons[] = compact('text', 'callback_data');

        $this->payload['reply_markup'] = json_encode([
            'inline_keyboard' => array_chunk($this->buttons, $columns),
        ], JSON_THROW_ON_ERROR);

        return $this;
    }

    /**
     * Send the message silently.
     * Users will receive a notification with no sound.
     *
     * @return static
     */
    public function disableNotification(bool $disableNotification = true): self
    {
        $this->payload['disable_notification'] = $disableNotification;

        return $this;
    }

    /**
     * Additional options
     *
     * @return static
     */
    public function options(array $options): self
    {
        $this->payload = array_merge($this->payload, $options);

        return $this;
    }

    /**
     * Get payload value by given key
     *
     * @return null|mixed
     */
    public function getPayloadValue(string $key)
    {
        return $this->payload[$key] ?? null;
    }

    /**
     * Returns params payload as array
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->payload;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
