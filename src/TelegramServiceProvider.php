<?php

namespace NotificationChannels\Telegram;

use Illuminate\Notifications\ChannelManager;
use Illuminate\Support\Facades\Notification;
use NotificationChannels\Telegram\Objects\Channel;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

/**
 * Class TelegramServiceProvider.
 *
 * @package NotificationChannels\Telegram
 * @author  Konstantin #MihaKot# Aksarin  <mihakot@gmail.com>
 *
 */
class TelegramServiceProvider extends PackageServiceProvider
{
    /**
     * install package
     */
    public function configurePackage(Package $package): void
    {
        $package
            ->name('notification-telegram')
            ->hasConfigFile();
    }

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/telegram.php' => config_path('telegram.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/telegram.php', 'telegram');

        $this->app->bind(Telegram::class, static fn() => new Telegram());

        Notification::resolved(static function (ChannelManager $service) {
            $service->extend('telegram', static fn($app) => $app->make(Channel::class));
        });
    }
}
