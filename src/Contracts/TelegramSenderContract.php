<?php

namespace NotificationChannels\Telegram\Contracts;

use NotificationChannels\Telegram\Exceptions\CouldNotSendNotification;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Telegram\Contracts\TelegramSenderContract
 *
 * @package NotificationChannels\Telegram
 * @author  Konstantin #MihaKot# Aksarin  <mihakot@gmail.com>
 */
interface TelegramSenderContract
{
    /**
     * Send the message.
     *
     *
     * @throws CouldNotSendNotification
     */
    public function send();
}
