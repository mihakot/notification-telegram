<?php

namespace NotificationChannels\Telegram\Objects;

use GuzzleHttp\Exception\GuzzleException;
use NotificationChannels\Telegram\Telegram;

/**
 * Class Telegram\Objects\Updates
 *
 * @package NotificationChannels\Telegram
 * @author  Konstantin #MihaKot# Aksarin  <mihakot@gmail.com>
 */
class Updates extends Telegram
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Telegram updates limit
     *
     * @return $this
     */
    public function limit(int $limit): self
    {
        $this->payload['limit'] = $limit;

        return $this;
    }

    /**
     * Get latest updates
     *
     * @param int $offset Identifier of the first update to be returned
     * @return $this
     * @see https://core.telegram.org/bots/api#getupdates
     */
    public function latest(int $offset = -1): self
    {
        $this->payload['offset'] = $offset;

        return $this;
    }

    /**
     * Get updates from Telegram
     *
     * @return array
     * @throws \JsonException
     * @throws \NotificationChannels\Telegram\Exceptions\CouldNotSendNotification
     * @throws GuzzleException
     */
    public function get(): array
    {
        $response = $this->sendRequest('getUpdates', $this->payload);

        return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
    }
}
